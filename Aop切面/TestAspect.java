package com.aaa;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AopAspect {
    // 配置织入点
    @Pointcut("@annotation(com.ruoyi.common.annotation.aop)")
    public void AopAspect() {
        System.out.println("配置织入点...");
    }

    @Before("AopAspect()")
    public void doBefore(JoinPoint joinPoint) {

        //设置参数
        Object[] args = joinPoint.getArgs();//参数值
        String[] argNames = ((MethodSignature)joinPoint.getSignature()).getParameterNames(); // 参数名

        for (int i=0;i<args.length;i++) {
            Object object = args[i];
            if (object == null) continue;
            System.out.println("参数切面输出:" + argNames[i] + "~~~" + object);
        }
            System.out.println("拦截业务逻辑代码........");
    }
}