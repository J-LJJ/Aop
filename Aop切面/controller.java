package com.aaa;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;


@ApiOperation(value = "aop")
@Log(title = "aop",operatorType= OperatorType.MOBILE)
@GetMapping("/aop.json")
@ResponseBody
@aop(param = "paramOne",param2 = "paramTwo")
public void aop(
		@ApiParam(value = "参数1",required = true)String paramOne
		,@ApiParam(value = "参数2",required = true)String paramTwo
){
	System.out.println("controller 业务");
}